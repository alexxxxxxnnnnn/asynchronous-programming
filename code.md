```
import kotlinx.coroutines.*
import java.net.HttpURLConnection
import java.net.URL

fun main() = runBlocking {
    val websites = listOf(
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.github.com",
        "https://www.twitter.com",
        "https://www.instagram.com",
    )

    val jobs = websites.map { url ->
        async {
            checkWebsite(url)
        }
    }

    jobs.awaitAll().forEachIndexed { index, result ->
        val url = websites[index]
        val status = if (result) "доступен" else "недоступен"
        println("Сайт $url $status")
    }
}

suspend fun checkWebsite(url: String): Boolean = withContext(Dispatchers.IO) {
    try {
        val connection = URL(url).openConnection() as HttpURLConnection
        connection.requestMethod = "HEAD"
        connection.connectTimeout = 5000
        connection.readTimeout = 5000
        connection.responseCode == HttpURLConnection.HTTP_OK
    } catch (e: Exception) {
        false
    }
}
```
